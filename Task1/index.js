const express = require("express");

const app = express();
const PORT = 8000;

app.get("/:echo", (req, res) => {
    res.send(req.params.echo)
});

app.listen(PORT, () => {
    console.log("We are live on " + PORT);
});