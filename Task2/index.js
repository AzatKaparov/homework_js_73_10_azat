const express = require("express");
const Vigenere = require('caesar-salad').Vigenere;

const app = express();
const PORT = 8000;
const password = "password";

app.get("/encode/:message", (req, res) => {
    const response = Vigenere.Cipher(password).crypt(req.params.message);
    res.send(response);
});

app.get("/decode/:message", (req, res) => {
    const response = Vigenere.Decipher(password).crypt(req.params.message);
    res.send(response);
});

app.listen(PORT, () => {
    console.log("We are live on " + PORT);
});